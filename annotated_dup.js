var annotated_dup =
[
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "imu", null, [
      [ "IMU", "classimu_1_1IMU.html", "classimu_1_1IMU" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_controller", null, [
      [ "Task_Controller", "classtask__controller_1_1Task__Controller.html", "classtask__controller_1_1Task__Controller" ]
    ] ],
    [ "task_imu", null, [
      [ "Task_IMU", "classtask__imu_1_1Task__IMU.html", "classtask__imu_1_1Task__IMU" ]
    ] ],
    [ "task_motor", null, [
      [ "Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_TP", null, [
      [ "Task_TP", "classtask__TP_1_1Task__TP.html", "classtask__TP_1_1Task__TP" ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ],
    [ "touchpanel", null, [
      [ "TouchPanel", "classtouchpanel_1_1TouchPanel.html", "classtouchpanel_1_1TouchPanel" ]
    ] ]
];