var main_8py =
[
    [ "duty", "main_8py.html#ad05e22ee18508db3285e78ac3c7eae94", null ],
    [ "IN1", "main_8py.html#a18a71c59d98e1a0f7a34ddeea2b4106a", null ],
    [ "IN2", "main_8py.html#a0178845387f5801fc517d235c178fe9d", null ],
    [ "IN3", "main_8py.html#a0cf680d768ccef3ce07fea95f74ba985", null ],
    [ "IN4", "main_8py.html#a5fb80a28e6382bc2611a8cb166efd70a", null ],
    [ "motor_1", "main_8py.html#a8c6b788401e5193e85faa861047749f4", null ],
    [ "motor_2", "main_8py.html#ad7ac91a26b464ff7e43c69c29fe5dc2d", null ],
    [ "motor_drv", "main_8py.html#a8c58cedb17222e4ad6d52cb03dd14ea2", null ],
    [ "panel_vector", "main_8py.html#ac0e71336eb548f531e8557cffc1c13e1", null ],
    [ "pinA0", "main_8py.html#a332c019d289de3578a89fdede6083ae1", null ],
    [ "pinA1", "main_8py.html#aecdfbbdc0117a73ad24ae58e4e93f11e", null ],
    [ "pinA6", "main_8py.html#a3a54a803b4eb7a6a97bbc6a70bc94a32", null ],
    [ "pinA7", "main_8py.html#a7c5bd8e8b652fb573807ad4c01a4f65f", null ],
    [ "run_flag", "main_8py.html#a45050bcecc316b7a30644c80cc48d541", null ],
    [ "task1", "main_8py.html#af4b8f4290f8d32e70654f6deb864787f", null ],
    [ "task2", "main_8py.html#a99d189b8f9eb3784cf7d4e5c3241b562", null ],
    [ "task3", "main_8py.html#a120516a5d42c212c898d4616d266077a", null ],
    [ "task5", "main_8py.html#a11ec9e490c402876467b9fdadecc93e8", null ],
    [ "task6", "main_8py.html#a4ff32207e9201d99684e38af7bee4d5d", null ],
    [ "touchpanel", "main_8py.html#a539db80a3b0a8fa6060a185be87f67dd", null ],
    [ "xm", "main_8py.html#a4832942da3cfa84771d7ebd0e31d8212", null ],
    [ "xp", "main_8py.html#abdc5f29a03afb363c3f16e482bf2279a", null ],
    [ "ym", "main_8py.html#af83aa58f6869bfe1860f14ed5516e0fd", null ],
    [ "yp", "main_8py.html#aff3e26187dfcc8c42a53447e2475397c", null ]
];